PRODUCT_SOONG_NAMESPACES += \
    vendor/MiuiCamera

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/MiuiCamera/proprietary/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,vendor/MiuiCamera/proprietary/system/priv-app/MiuiCamera/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib) \
    $(call find-copy-subdir-files,*,vendor/MiuiCamera/proprietary/system/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64) \
    $(call find-copy-subdir-files,*,vendor/MiuiCamera/proprietary/system_ext/lib64,$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64) \
    $(call find-copy-subdir-files,*,vendor/MiuiCamera/proprietary/vendor/lib,$(TARGET_COPY_OUT_VENDOR)/lib) \
    $(call find-copy-subdir-files,*,vendor/MiuiCamera/proprietary/vendor/lib64,$(TARGET_COPY_OUT_VENDOR)/lib64)

PRODUCT_PACKAGES += \
    MiuiCamera \
    MiuiCameraOverlay

PRODUCT_PROPERTY_OVERRIDES += \
    ro.miui.notch=1 \
    ro.hardware.camera=xiaomi

PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.lens.oem_camera_package=com.android.camera
